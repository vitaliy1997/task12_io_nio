package ua.com.nio.droids;

import ua.com.nio.ship.Ship;

import java.io.Serializable;

public class DroidUnoImpl implements Serializable {

    private transient Ship ship;
    private String model;
    private int year;

    public DroidUnoImpl(String model, int year, Ship ship) {
        this.model = model;
        this.year = year;
        this.ship = ship;
    }

    @Override
    public String toString() {
        return "DroidUno{" +
                "ship=" + ship +
                ", model='" + model + '\'' +
                ", year=" + year +
                '}';
    }
}
