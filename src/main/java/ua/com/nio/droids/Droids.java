package ua.com.nio.droids;

public interface Droids {

   public String model();
   public int year();

}
