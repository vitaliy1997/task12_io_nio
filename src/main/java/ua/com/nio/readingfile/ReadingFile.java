package ua.com.nio.readingfile;

import java.io.*;

public class ReadingFile {

    public void usualReading() throws IOException {

        int count = 0;
        System.out.println("****Usual*****");
        InputStream input =
                new FileInputStream("c:\\File200.exe");
        int data = input.read();
        while (data != -1) {
            data = input.read();
            count++;
        }
        input.close();
        System.out.println("count" + count);
        System.out.println("*****END****");
    }

    public void bufferReading() throws IOException {
        int count = 0;
        System.out.println("*****Buffer*****");
        DataInputStream dataInput = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("c:\\File200.exe")));

        try {
            while (true) {
                byte b = dataInput.readByte();
                count++;
            }
        }catch (IOException e){

        }
        dataInput.close();
        System.out.println("count:"+count);
        System.out.println("*****End*****");
    }
}
