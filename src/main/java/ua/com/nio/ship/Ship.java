package ua.com.nio.ship;

import java.io.Serializable;

public class Ship implements Serializable {

    private String nameShip;
    private int numDroids;


    public Ship(String nameShip, int numDroids) {
        this.nameShip = nameShip;
        this.numDroids = numDroids;
    }


    public String getNameShip() {
        return nameShip;
    }

    public int getNumDroids() {
        return numDroids;
    }
}
