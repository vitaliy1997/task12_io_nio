package ua.com.nio.view;

import ua.com.nio.droids.DroidDosImpl;
import ua.com.nio.droids.DroidUnoImpl;
import ua.com.nio.ship.Ship;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DroidsView {

    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Views> putMenu = new LinkedHashMap<>();
    Ship ship = new Ship("1000year Sokol", 12);
    DroidUnoImpl droidUno = new DroidUnoImpl("C3-PO", 2001, ship);
    DroidDosImpl droidDos = new DroidDosImpl("R2-D2", 2004, ship);
    Scanner scanner = new Scanner(System.in);

    public DroidsView() {

        menu.put("1", "1-Output serializable");
        menu.put("2", "2-Input serializable");
        menu.put("E", "E-Exit");

        putMenu.put("1", this::putNum1);
        putMenu.put("2", this::putNum2);
    }

    private void putNum1() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("DroidsOnShip"));
        out.writeObject(droidUno);
        out.writeObject(droidDos);
        out.close();
        System.out.println("Before: \n" + droidUno);
        System.out.println(droidDos);

    }

    private void putNum2() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("DroidsOnShip"));
        droidUno = (DroidUnoImpl) in.readObject();
        droidDos = (DroidDosImpl) in.readObject();
        System.out.println("After: \n" + droidUno);
        System.out.println(droidDos);
    }

    //----------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}
