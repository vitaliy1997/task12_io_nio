package ua.com.nio.view;

import ua.com.nio.droids.DroidDosImpl;
import ua.com.nio.droids.DroidUnoImpl;
import ua.com.nio.ship.Ship;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    ViewReadingFile viewReadingFile;
    DroidsView droidsView ;
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Views> putMenu = new LinkedHashMap<>();

    Scanner scanner = new Scanner(System.in);

    public MainView() {

        menu.put("1", "1-DroidsView");
        menu.put("2", "2-ViewReadingFile");
        menu.put("E", "E-Exit");

        putMenu.put("1", this::putNum1);
        putMenu.put("2", this::putNum2);
    }

    private void putNum2() {
        viewReadingFile=new ViewReadingFile();
        viewReadingFile.show();
    }

    private void putNum1() {
        droidsView=new DroidsView();
        droidsView.show();

    }

    //________________________________________________________


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}
