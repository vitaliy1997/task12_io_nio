package ua.com.nio.view;

import ua.com.nio.readingfile.ReadingFile;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewReadingFile {

    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Views> putMenu = new LinkedHashMap<>();
    Scanner scanner = new Scanner(System.in);
    ReadingFile readingFile = new ReadingFile();

    public ViewReadingFile() {

        menu.put("1", "1-Usual Reading");
        menu.put("2", "2-Buffered Reading");
        menu.put("E", "E-Exit");

        putMenu.put("1", this::putNum1);
        putMenu.put("2", this::putNum2);
    }


    private void putNum1() {
        try {
            readingFile.usualReading();
        } catch (IOException e) {

        }
    }

    private void putNum2() {
        try {
            readingFile.bufferReading();
        }catch (IOException e){

        }
    }

    //______________________________________________________________________


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }


}
