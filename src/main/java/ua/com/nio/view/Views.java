package ua.com.nio.view;

import java.io.IOException;

@FunctionalInterface
public interface Views {

    void print() throws IOException, ClassNotFoundException;
}
